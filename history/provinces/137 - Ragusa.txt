#137 - Ragusa | 

owner = A64
controller = A64
add_core = A64
add_core = A63
culture = redfoot_halfling
religion = regent_court

hre = no

base_tax = 2
base_production = 3
base_manpower = 2

trade_goods = livestock
capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_kobold
