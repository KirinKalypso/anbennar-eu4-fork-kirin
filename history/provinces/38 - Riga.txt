#Riga Bight, incl. the city of Riga, and D�nam�nde.

owner = A01
controller = A01
add_core = A01
culture = high_lorentish
religion = regent_court
base_tax = 7
base_production = 5
trade_goods = wine
base_manpower = 4
capital = ""
is_city = yes
hre = no

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
extra_cost = 10
