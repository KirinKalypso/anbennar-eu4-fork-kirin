#163 - Crete | 

owner = A64
controller = A64
add_core = A64
culture = redfoot_halfling
religion = regent_court

hre = no

base_tax = 3
base_production = 2
base_manpower = 1

trade_goods = grain
capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_kobold
