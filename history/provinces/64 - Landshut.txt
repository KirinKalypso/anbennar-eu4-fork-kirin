#64 - Landshut | 

owner = A20
controller = A20
add_core = A20
culture = ruby_dwarf
religion = ancestor_worship

hre = no

base_tax = 9
base_production = 6
base_manpower = 4

trade_goods = iron
capital = ""

is_city = yes
fort_15th = yes 

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish