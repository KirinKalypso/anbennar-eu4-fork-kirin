# No previous file for Sukhothai
owner = F39
controller = F39
add_core = F39
culture = zanite
religion = bulwari_sun_cult

hre = no

base_tax = 8
base_production = 10
base_manpower = 5

trade_goods = silk

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish
discovered_by = tech_harpy
discovered_by = tech_goblin
discovered_by = tech_bulwari