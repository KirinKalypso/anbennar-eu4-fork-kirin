government = imperial_city
government_rank = 1
mercantilism = 25
primary_culture = anbenncoster
religion = regent_court
technology_group = tech_cannorian
national_focus = DIP
capital = 8 #its Anbenncost
fixed_capital = 8 # Cannot move capital away from this province & no power cost to move to it

1422.1.1 = { set_country_flag = lilac_wars_moon_party }


1440.1.1 = {
	monarch = {
		name = "City Council"
		adm = 2
		dip = 2
		mil = 2
		regent = yes
	}
}