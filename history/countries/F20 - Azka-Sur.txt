government = despotic_monarchy
government_rank = 1
primary_culture = zanite
add_accepted_culture = sun_elf
religion = bulwari_sun_cult
technology_group = tech_bulwari
historical_friend = F25 #historical overlord
capital = 643

1444.1.1 = {
	monarch = {
		name = "Mardint"
		dynasty = "szel-Sur"
		birth_date = 1401.7.3
		adm = 3
		dip = 4
		mil = 2
		culture = zanite
	}
	add_ruler_personality = naive_personality
}
