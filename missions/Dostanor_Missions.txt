rebuilding_corveld = {
	slot = 1
	generic = no
	ai = yes
	potential = {
		tag = Z07
	}
	has_country_shield = yes
	
	eliminating_the_opposition = {
		icon = mission_unite_home_region
		required_missions = { }
		position = 1
		provinces_to_highlight = {
			province_id = 451
			area = crathanor_area
			NOT = { 
				country_or_non_sovereign_subject_holds = ROOT 
			}
		}
		trigger = {
			owns_core_province = 451
			crathanor_area = {
				type = all
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		effect = { 
			447 = {
				add_base_tax = 3
				add_base_production = 3
				add_base_manpower = 3
			}
		}
	}
	expand_our_trade_network = {
		icon = mission_dominate_home_trade_node
		required_missions = { eliminating_the_opposition }
		position = 2
		trigger = {
			gablaine_trade_node = {
				trade_share = {
					country = ROOT
					share = 75
				}
			}
		}
		effect = {
			447 = {
				add_base_tax = 2
				add_base_production = 3
				add_base_manpower = 1
			}
		}
	}
	the_northern_way = {
		icon = mission_trade_company_region_abroad
		required_missions = { 
			expand_our_trade_network
			uniting_dostanor
		}
		position = 3
		trigger = {
			marches_trade_nodes = {
				trade_share = {
					country = ROOT
					share = 75
				}
			}
		}
		effect = { 
			447 = {
				add_base_tax = 1
				add_base_production = 3
				add_base_manpower = 2
			}
			south_castanor_region = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = ROOT
			}
		}
	}
	the_jewel_of_the_swamp = {
		icon = mission_war_chest
		required_missions = { the_northern_way }
		position = 6
		trigger = {
			447 = {
				development = 40
				has_building =  trade_depot
			}
		}
		effect = {
			447 = {
				add_province__modifier = {
					name = "center_of_trade_modifier
					duration = -1
				}
			}
		}
	}
}

z07_trade_expansion_1 = {
	slot = 2
	generic = no
	ai = yes 
	potential = {
		tag = Z07 
	}
	has_country_shield = yes
	
	to_the_east = {
		icon = mission_establish_high_seas_navy
		required_missions = { expand_our_trade_network }
		position = 4
		trigger = {
			ovdal_tungr_trade_node = {
				country = ROOT
				share = 75
			}
		}
		effect = {
			447 = {
				add_base_tax = 2
				add_base_production = 3
				add_base_manpower = 1
			}
		}
	}
}

z07_trade_expansion_2 = {
	slot = 3
	generic = no
	ai = yes 
	potential = {
		tag = Z07 
	}
	has_country_shield = yes

	western_trade = {
		icon = dominate_home_trade_node
		required_missions = { expand_our_trade_network }
		position = 5
		trigger = {
			eborthil_trade_node = {
				country = ROOT
				share = 75
			}
		}
		effect = {
			447 = {
				add_base_tax = 1
				add_base_production = 3
				add_base_manpower = 2
			}
		}
	}
}

dostanorian_expansion_1 = {
	slot = 4
	generic = no
	ai = yes 
	potential = {
		tag = Z07 
	}
	has_country_shield = yes

	uniting_dostanor = {
		icon = mission_have_two_subjects
		required_missions = { }
		position = 1
		provinces_to_highlight = {
			OR = {
				region = daravans_folly_region
				region = dostanor_region
				region = overmarch_region
			}
			NOT = {
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		trigger = { 
			daravans_folly_region = {
				type = all
				country_or_non_sovereign_subject_holds = ROOT
			}
			dostanor_region = {
				type = all
				country_or_non_sovereign_subject_holds = ROOT
			}
			overmarch_region = {
				type = all
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		effect = {
			add_mil_power = 100
			add_prestige = 20
			add_legitimiacy = 20
		}
	}
}